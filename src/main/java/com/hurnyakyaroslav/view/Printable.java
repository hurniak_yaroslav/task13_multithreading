package com.hurnyakyaroslav.view;

@FunctionalInterface
public interface Printable {
    void print();
}
