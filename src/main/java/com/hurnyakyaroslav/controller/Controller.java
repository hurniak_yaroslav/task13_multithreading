package com.hurnyakyaroslav.controller;

import com.hurnyakyaroslav.model.Model;

public class Controller {

    Model model = new Model();

    public String pingPong() {
        return model.pingPong();
    }

    public void simpleFibonacci() {
        model.fibonacci();
    }

    public void executorsFibonacci() {
        model.executorsWithFibonacci();
    }

    public void testScheduledThreadPool(int quantity) {
        model.testScheduledThreadPool(quantity);
    }

    public void testSynchronized() {
        Thread main = Thread.currentThread();
        try {
            main.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        model.testSynchronizedOnObject();
        main.notify();
    }

    public void testLocks(){
        model.testLocks();
    }

}
